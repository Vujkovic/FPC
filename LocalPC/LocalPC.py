import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib
import seaborn as sns
import sklearn
import imblearn
import matplotlib.pyplot as plt
import time
import sklearn.metrics as m
import math

# Ignore warnings
import warnings
warnings.filterwarnings('ignore')

#Settings
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

#Load Data
df =pd.read_csv("local.pcap_Flow.csv")

df.info()
df.head()

df['Label'].value_counts()

df.drop(['Flow ID'], axis=1, inplace=True)
df.drop(['Src IP'], axis=1, inplace=True)
df.drop(['Dst IP'], axis=1, inplace=True)
df.drop(['Timestamp'], axis=1, inplace=True)
df.drop(['Label'], axis=1, inplace=True)

df.describe()
    
#Date preprocesing
df.drop(['Active Min'], axis=1, inplace=True)
df.drop(['Active Max'], axis=1, inplace=True)
df.drop(['Active Std'], axis=1, inplace=True)
df.drop(['Active Mean'], axis=1, inplace=True)
df.drop(['Subflow Bwd Pkts'], axis=1, inplace=True)
df.drop(['Bwd Byts/b Avg'], axis=1, inplace=True)
df.drop(['Fwd Blk Rate Avg'], axis=1, inplace=True)
df.drop(['Fwd Pkts/b Avg'], axis=1, inplace=True)
df.drop(['Fwd Byts/b Avg'], axis=1, inplace=True)
df.drop(['ECE Flag Cnt'], axis=1, inplace=True)
df.drop(['CWE Flag Count'], axis=1, inplace=True)
df.drop(['URG Flag Cnt'], axis=1, inplace=True)
df.drop(['Bwd URG Flags'], axis=1, inplace=True)
df.drop(['Fwd URG Flags'], axis=1, inplace=True)
df.drop(['Bwd PSH Flags'], axis=1, inplace=True)

df.info()

X = df.values

#Evaluate Models
#elbow
from sklearn.cluster import KMeans
wcss = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters = i, init = 'k-means++', random_state = 42)
    kmeans.fit(X)
    wcss.append(kmeans.inertia_)
plt.plot(range(1, 11), wcss)
plt.title('The Elbow Method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.show()

# Fitting K-Means to the dataset
kmeans = KMeans(n_clusters = 3, init = 'k-means++', random_state = 42)
y_kmeans = kmeans.fit_predict(X)

a=0
b=1
#Desired plot visualisation
plt.scatter(X[y_kmeans == 0, a], X[y_kmeans == 0, b], s = 1, c = 'red', label = 'Cluster 1')
plt.scatter(X[y_kmeans == 1, a], X[y_kmeans == 1, b], s = 1, c = 'blue', label = 'Cluster 2')
plt.scatter(X[y_kmeans == 2, a], X[y_kmeans == 2, b], s = 1, c = 'green', label = 'Cluster 3')
plt.scatter(kmeans.cluster_centers_[:, a], kmeans.cluster_centers_[:, b], s = 50, c = 'yellow', label = 'Centroids')
plt.title('Clusters from {}, {}'.format(df.columns[a],df.columns[b]))
plt.xlabel('{}'.format(df.columns[a]))
plt.ylabel('{}'.format(df.columns[b]))
plt.legend()
plt.show()

#All plots
for i in range(len(df.columns)):
    for j in range(i+1,len(df.columns)):
        plt.scatter(X[y_kmeans == 0, i], X[y_kmeans == 0, j], s = 1, c = 'red', label = 'Cluster 1')
        plt.scatter(X[y_kmeans == 1, i], X[y_kmeans == 1, j], s = 1, c = 'blue', label = 'Cluster 2')
        plt.scatter(X[y_kmeans == 2, i], X[y_kmeans == 2, j], s = 1, c = 'green', label = 'Cluster 3')
        plt.scatter(kmeans.cluster_centers_[:, i], kmeans.cluster_centers_[:, j], s = 50, c = 'yellow', label = 'Centroids')
        plt.title('Clusters from {}, {}'.format(df.columns[i],df.columns[j]))
        plt.xlabel('{}'.format(df.columns[i]))
        plt.ylabel('{}'.format(df.columns[j]))
        plt.legend()
        plt.show()