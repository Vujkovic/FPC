import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = ('Naive Bayes', 'Decision Tree', 'K Neighbours', 'Logistic Regression')
y_pos = np.arange(len(objects))
performance = [0.8,0.9996,0.9990,0.89]

plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects)
plt.ylabel('Score')
plt.title('Accuracy')
plt.ylim([0.75,1])

plt.show()